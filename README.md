# ICCS19 Redescriptions

This project provides raw data and results of the experiments ran for ICCS 2019.

Experiments were run with the algorithm `ReReMi` via [Siren](http://siren.gforge.inria.fr/main/)([paper](https://hal.archives-ouvertes.fr/hal-01399211)).
## Settings
### Raw datasets
The datasets are extracted from [DBpedia](https://wiki.dbpedia.org/). They are provided as raw data (RDF triples).
The info file provides the SPARQL query used and some basic statistics.

* `Turing_Award_laureates`
* `Smartphones`
* `Sport_cars`
* `French_films`

### ReReMi inputs

The file `reremi_conf.xml` is the configuration file of ReReMi used for the extraction of the redescriptions.
For each dataset, the two input files (categories and descriptions) are provided in a `zip` archive.

## Results & Evaluation

### Definitions

For each dataset, a `csv` file provides the redescriptions obtained. :
 
* eval : Evaluation from the experts (0/1)
* query_LHS_named : Category defined
* query_RHS_named : Descriptors of the query
* acc : Jaccard coefficient
* pval : p-value of the rule
* card_E_ij : Number of objects satisfying query_LHS_named, query_RHS_named, both queries or none of them.

### Incompatible categories

For each dataset, a `csv` file is provided with the two incompatible categories.

## How to reproduce the experiment

1. Download and install `Siren` ([website](http://siren.gforge.inria.fr/main/))
2. Import data (File > Import > Import data)
    * For the left hand side, select `<dataset>_ctxt_cat.csv`
    * For the right hand side, select `<dataset>_ctxt_descr.csv`
    * Set the Delimiter at `TAB`
3. Import preferences (File > Import > Import preferences)
    * Select `reremi_conf.xml`
4. Mine redescriptions (Process > Mine redescriptions)

*Remark*: the dataset `French_films` may take a while to be improted and processed